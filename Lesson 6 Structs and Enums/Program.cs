﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_6_Structs_and_Enums
{
    class Program
    {
        static void Main(string[] args)
        {
            HttpCodes code = HttpCodes.NotFound;
            //int number = 500;
            HttpCodes result;
            if (Enum.TryParse("500", out result))
            {
                Console.WriteLine(result);
            }

            Person person = new Person();
            person.FullName = null;
            person.Age = null;
            person.BirthDate = null;
            Point point = new Point(0, 0);

            Console.ReadLine();
        }
    }
}
