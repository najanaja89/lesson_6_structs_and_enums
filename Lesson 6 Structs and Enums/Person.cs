﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_6_Structs_and_Enums
{
    public class Person
    {
        public string FullName { get; set; }
        public Nullable<DateTime> BirthDate { get; set; }
        //или
        public int? Age { get; set; }
    }
}
