﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_6_Structs_and_Enums
{
    public enum HttpCodes
    {
        NotFound = 404,
        NotAuthorized = 401,
        InternalServerError = 500
    }
}
